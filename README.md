# ExamenPractico

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.2.

## Instalación y puesta en marcha
Una vez descargado el proyecto debes moverte a su directorio y correr `npm install`
Luego debes ejecutar `ng serve --open` esto abrirá automaticamente una ventana en tu navegador con el sistema.

## Build

Ejecutar `ng build --prod` para compilar el proyecto. 