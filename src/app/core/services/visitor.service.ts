import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable, } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { VisitorResponseDTO } from 'src/app/shared/models/vistor';

const VISITOR = 'visitor';
@Injectable({
  providedIn: 'root'
})
export class VisitorService {

  constructor(
    private http: HttpClient
  ) { }

  public getVisitor(): Observable<VisitorResponseDTO> {
    return this.http.post<VisitorResponseDTO>(environment.apiURL.concat(VISITOR), {}).pipe(map(response => this.fixWelcomeName(response))
    );
  }

  fixWelcomeName(response) {
    response.data.welcome = response.data.welcome.replace('Hola, ', '');
    response.data.version = response.data.version.replace('v.', 'versión ');
    return response;
  }
}
