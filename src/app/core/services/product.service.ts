import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable, } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { VisitorResponseDTO } from 'src/app/shared/models/vistor';
import { ProductResponseDTO } from '../../shared/models/product.d';

const PRODUCT = 'productos';
@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(
    private http: HttpClient
  ) { }

  public getProducts(name, numberPage): Observable<ProductResponseDTO> {
    return this.http.get<ProductResponseDTO>(environment.apiURL.concat(PRODUCT)
        .concat(`/${name}/${numberPage}`))
        .pipe(map(response => response)
    );
  }
}
