import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable, throwError, from} from 'rxjs';
import {catchError, map, mergeMap} from 'rxjs/operators';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor {
  myToken: any;
  loaderToShow: any;

  constructor(
    private router: Router,
  ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!request.headers.has('Content-Type')) {
      request = request.clone({
        headers: request.headers.set('Content-Type', 'application/json')
      });
    }
    if (!request.headers.has('Accept')) {
      request = request.clone({
        headers: request.headers.set('Accept', 'application/json'),
      });
    }
    return next.handle(request).pipe(
      map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
        }
        return event;
      }),
      catchError((error: HttpErrorResponse) => {
        if (error.status === 401) {
          localStorage.clear();
          this.router.navigate(['/login']);
        }

        if (error.status === 422) {
          let errorMessage = '';
          Object.entries(error.error.errors).forEach(([key, value]) => {
            errorMessage += value + '<br>';
            }
          );

          // this.commonService.showToast(errorMessage);
        }

        if (error.status === 500) {
          if (!error.error || typeof error.error !== 'object') {
            // this.commonService.showToast('Oh vaya, es vergonzoso, pero ocurrio un error desconocido.');
          } else {
            let errorMessage = '';
            if (error.error.error.code !== 200) {
              if (error.error.error.validationErrors) {
                Object.entries(error.error.error.validationErrors).forEach(([key, value]) => {
                  errorMessage += value + '<br>';
                  }
                );
                // this.commonService.openSnackbar(errorMessage);
              } else {
                // this.commonService.openSnackbar(((error.error.error.details) ? error.error.error.details : error.error.error.message));
              }
            }
          }
        }


        if (error.status === 503) {
          // this.commonService.showToast('Disculpe ocurrio un error en su solicitud, por favor intente nuevamente');
        }

        if (error.status === 400) {
          let errorMessage = '';
          if (error.error.error.validationErrors) {
            Object.entries(error.error.error.validationErrors).forEach(([key, value]) => {
              const data: any = value;
              errorMessage += data.message + '<br>';
              }
            );
            // this.commonService.showToast(errorMessage);
          } else {
            // this.commonService.showToast((error.error.error.details) ? error.error.error.details : error.error.error.message);
          }

        }
        return throwError(error);
      }));
  }
}
