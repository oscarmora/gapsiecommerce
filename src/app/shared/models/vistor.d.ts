export interface VisitorResponseDTO {
  code?:        number;
  description?: string;
  data?:        Visitor;
}

export interface Visitor {
  type?:      string;
  visitorID?: string;
  welcome?:   string;
  version?:   string;
}
