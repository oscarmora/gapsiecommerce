export interface ProductResponseDTO {
  code?:        number;
  description?: string;
  data?:        Data;
}

export interface Data {
  type?:     string;
  products?: Product[];
}

export interface Product {
  ID?:          number;
  TYPE?:        string;
  SKU?:         string;
  NAME?:        string;
  DESCRIPTION?: string;
  PRICE?:       number;
  IMAGE?:       string;
}