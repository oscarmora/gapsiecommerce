import { Component, OnInit } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { VisitorService } from 'src/app/core/services/visitor.service';
import { Visitor } from 'src/app/shared/models/vistor';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {
  visitor: Visitor;
  loadingVisitor = true;

  constructor(
    private visitorService: VisitorService
  ) { }

  ngOnInit(): void {
    this.visitorService.getVisitor().subscribe(response => {
      this.visitor = response.data;
      this.loadingVisitor = false;
    }, error => this.loadingVisitor = false);
  }

}
