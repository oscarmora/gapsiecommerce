import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../core/services/product.service';
import { Product } from '../../shared/models/product.d';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  products: Array<Product> = [];
  nameProduct = '';
  numberPage = 1;
  cart = [];
  canLoadMore = false;
  loadingMore = false;
  loadingProducts = false;
  constructor(
    private productService: ProductService
  ) { }

  ngOnInit(): void {
  }

  findProducts(addedMore = false) {
    if (addedMore) {
      this.loadingMore = true;
    } else {
      this.loadingProducts = true;
    }
    this.productService.getProducts(this.nameProduct, this.numberPage).subscribe(response => {
      if (!response.data) {
        this.canLoadMore = false;
        return;
      }
      if (addedMore) {
        response.data.products.forEach(product => {
          this.products.push(product);
        });
      } else {
        this.products = response.data.products;
      }
      this.canLoadMore = this.products.length > 0 ? true : false;
      this.loadingMore = false;
      this.loadingProducts = false;
    }, error => {
      this.loadingMore = false;
      this.loadingProducts = false;
      this.products = [];
    });
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
                        event.container.data,
                        event.previousIndex,
                        event.currentIndex);
    }
  }

}
